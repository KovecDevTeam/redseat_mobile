import 'dart:math';

import 'package:flutter/material.dart';

import 'customClipper.dart';

class LogoContainer extends StatelessWidget {
  const LogoContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/images/logo.png',
      width: 250,
      height: 250,
      fit: BoxFit.contain,
    );
  }
}
