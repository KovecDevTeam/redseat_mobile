import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class PhoneNumberFieldWidget extends StatefulWidget {
  final TextEditingController controller;
  final String title;
  final Icon icon;
  const PhoneNumberFieldWidget(
      {Key? key,
      required this.controller,
      required this.title,
      required this.icon})
      : super(key: key);

  @override
  _PhoneNumberFieldWidgetState createState() => _PhoneNumberFieldWidgetState();
}

class _PhoneNumberFieldWidgetState extends State<PhoneNumberFieldWidget> {
  @override
  void initState() {
    super.initState();

    widget.controller.addListener(onListen);
  }

  @override
  void dispose() {
    widget.controller.removeListener(onListen);
    super.dispose();
  }

  void onListen() => setState(() {});

  @override
  Widget build(BuildContext context) => TextFormField(
        controller: widget.controller,
        decoration: InputDecoration(
          hintText: widget.title,
          labelText: widget.title,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          prefixIcon: widget.icon,
          suffixIcon: widget.controller.text.isEmpty
              ? Container(width: 0)
              : IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () => widget.controller.clear(),
                ),
        ),
        keyboardType: TextInputType.phone,
        autofillHints: [AutofillHints.telephoneNumber],
        autofocus: true,
        validator: (text) =>
            text != null && text.length < 5 ? 'Enter min. 5 characters' : null,
      );
}
