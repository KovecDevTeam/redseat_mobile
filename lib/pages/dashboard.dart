import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:ui';
import 'package:intl/intl.dart';

class DashBoard extends StatefulWidget {
  DashBoard({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() => _DashBoardState();
}

final formatCurrency = new NumberFormat.simpleCurrency();
Widget _addressCard() {
  return Container(
    height: 155,
    padding: const EdgeInsets.only(
      left: 30,
      right: 0,
      top: 20,
      bottom: 0,
    ),
    width: double.infinity,
    decoration: BoxDecoration(
      gradient: LinearGradient(
          begin: Alignment.center,
          end: Alignment(0.8, 0.0),
          colors: [
            Colors.pink.shade500,
            Colors.pinkAccent.shade400,
          ]),
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(50),
        topRight: Radius.circular(0),
        bottomLeft: Radius.circular(0),
        bottomRight: Radius.circular(50),
      ),
      color: Colors.white,
    ),
    child: Row(
      children: [
        Icon(
          Icons.location_on,
          color: Colors.white,
          size: 50,
        ),
        Expanded(
          /*1*/
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /*2*/
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Kevonia Tomlinson',
                  style: GoogleFonts.sora(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 24,
                          color: Colors.white)),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  '3712 East Industrial Way',
                  style: GoogleFonts.sora(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: Colors.white)),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Unit 7, RSC777',
                  style: GoogleFonts.sora(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          color: Colors.white)),
                ),
              ),
              Text(
                'Riviera Beach, Fl 33404',
                style: GoogleFonts.sora(
                    textStyle: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: Colors.white)),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget _detailCard(double screenwidth) {
  return Column(children: [
    Row(children: [
      Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
              width: screenwidth / 2.4,
              height: 80,
              child: Column(children: [
                Row(children: [
                  Container(
                      width: 60,
                      height: 80,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Colors.purple.shade900,
                            Colors.purpleAccent.shade700,
                          ]),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(0),
                            topRight: Radius.circular(0),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(35),
                          ),
                          color: Colors.black),
                      child: Icon(
                        Icons.paid,
                        color: Colors.white,
                        size: 50,
                      )),
                  Container(
                    padding: const EdgeInsets.only(left: 8),
                    child: Column(children: [
                      Text(
                        '${formatCurrency.format(1960.93)}',
                        style: GoogleFonts.sora(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w800,
                                fontSize: 16,
                                color: Colors.black)),
                      ),
                    ]),
                  ),
                ])
              ])),
        ),
      ),
      Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
              width: screenwidth / 2.4,
              height: 80,
              child: Column(children: [
                Row(
                  children: [
                    Container(
                        width: 60,
                        height: 80,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Colors.orange.shade900,
                              Colors.orange.shade700,
                            ]),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(0),
                              topRight: Radius.circular(0),
                              bottomLeft: Radius.circular(0),
                              bottomRight: Radius.circular(35),
                            ),
                            color: Colors.black),
                        child: Icon(
                          Icons.star,
                          color: Colors.white,
                          size: 50,
                        )),
                    Container(
                      padding: const EdgeInsets.only(left: 8),
                      child: Column(children: [
                        Text(
                          '500',
                          style: GoogleFonts.sora(
                              textStyle: TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 16,
                                  color: Colors.black)),
                        ),
                      ]),
                    ),
                  ],
                ),
              ])),
        ),
      ),
    ]),
    Row(children: [
      Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
              width: screenwidth / 2.4,
              height: 80,
              child: Column(children: [
                Row(children: [
                  Container(
                      width: 60,
                      height: 80,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Colors.red,
                            Colors.redAccent.shade700,
                          ]),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(0),
                            topRight: Radius.circular(0),
                            bottomLeft: Radius.circular(0),
                            bottomRight: Radius.circular(35),
                          ),
                          color: Colors.black),
                      child: Icon(
                        Icons.train,
                        color: Colors.white,
                        size: 50,
                      )),
                  Container(
                    padding: const EdgeInsets.only(left: 8),
                    child: Column(children: [
                      Text(
                        '0',
                        style: GoogleFonts.sora(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w800,
                                fontSize: 16,
                                color: Colors.black)),
                      ),
                    ]),
                  ),
                ])
              ])),
        ),
      ),
      Card(
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
              width: screenwidth / 2.4,
              height: 80,
              child: Column(children: [
                Row(
                  children: [
                    Container(
                        width: 60,
                        height: 80,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                              Colors.blue,
                              Colors.blue.shade700,
                            ]),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(0),
                              topRight: Radius.circular(0),
                              bottomLeft: Radius.circular(0),
                              bottomRight: Radius.circular(35),
                            ),
                            color: Colors.black),
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 50,
                        )),
                    Container(
                      padding: const EdgeInsets.only(left: 8),
                      child: Column(children: [
                        Text(
                          '0',
                          style: GoogleFonts.sora(
                              textStyle: TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 16,
                                  color: Colors.black)),
                        ),
                      ]),
                    ),
                  ],
                ),
              ])),
        ),
      ),
    ]),
  ]);
}

class _DashBoardState extends State<DashBoard> {
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 50),
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.grey.shade200,
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _addressCard(),
              SizedBox(height: 20),
              _detailCard(width)
            ],
          ),
        ),
      ),
    );
  }
}
